# ArkanoidClone

Dies ist mein Arkanoid Clone für die erste Abgabe der Vorlesung "Game Engines".

In meiner Umsetzung funktioniert das grundlegende Gameplay wie in der Aufgabenstellung gewünscht.
Gesteuert wird das Paddle mit "A" + "D" oder den Pfeiltasten. Der Ball kann mit der Leertaste zu Beginn und nach jedem verlorenen Leben vom Paddle
"losgelassen" werden. Somit kann ein Spieler selbst entscheiden, an welcher Position und wann er das Level beginnen möchte (siehe Video).

Die Farben der Steine zeigen bei mir an, wie viele Trefferpunkte ein Stein noch besitzt. Grün steht dabei für drei übrige Trefferpunkte,
gelb für zwei und rot benötigt nur noch einen Treffer, um den Stein zu zerstören. Bei einem Treffer wechselt ein Stein die Farbe entsprechend der
noch vorhandenen Trefferpunkte.

Bei meiner Version gibt es drei verschiedene Powerups, die vom Spieler eingesammelt werden können. Powerup-Effekte können mehrfach wirken,
gehen allerdings verloren, sollte man ein Leben verlieren. Die Powerups spawnen mit einer Wahrscheinlichkeit von 33% nachdem ein Block zerstört wird.

Folgende Eigenschaften bringen diese Powerups:
    
- hellblau -> Erhöht die Bewegungsgeschwindigkeit des Paddles
- pink -> Vergrößert das Paddle
- dunkelblau ->  Verlangsamt die Geschwindigkeit des Balls

Punkte gibt es, wenn ein Stein zerstört wird und Leben kann man klassisch verlieren, wenn der Ball aus dem Spielfeld fliegt.

Optional habe ich noch einen kleinen Start- und Endscreen zur Übersichtlichkeit eingebaut.

Die verwendeten Sounds habe ich von folgender Website: https://www.sounds-resource.com/nes/arkanoid/sound/3698/
